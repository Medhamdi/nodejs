var express= require("express")
var adminController= require("../app/controller/adminController")
var router=express();
var verifyToken= require("../verifyToken")

router.post("/create",adminController.create)
//public routes
router.put("/update",verifyToken,adminController.update)
router.delete("/delete",verifyToken,adminController.delete)
router.get("/all",adminController.getAll)
router.post("/getByLastName",verifyToken,adminController.getByLastName)


router.post("/auth",adminController.auth)
module.exports=router


