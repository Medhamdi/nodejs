var express= require("express")
var benevoleController= require("../app/controller/benevoleController")
var router=express();


router.post("/create",benevoleController.create)
router.put("/update",benevoleController.update)
router.delete("/delete",benevoleController.delete)
router.get("/all",benevoleController.getAll)
router.post("/getByLastName",benevoleController.getByLastName)

module.exports=router


