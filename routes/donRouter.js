var express= require("express")
var donController= require("../app/controller/donController")
var router=express();


router.post("/create",donController.create)
router.put("/update",donController.update)
router.delete("/delete",donController.delete)
router.get("/all",donController.getAll)
router.post("/getByLastName",donController.getByLastName)

module.exports=router


