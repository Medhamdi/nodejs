var express= require("express")
var associationController= require("../app/controller/associationController")
var router=express();
var verifyToken= require("../verifyToken")

router.post("/create",associationController.create)

router.put("/update",verifyToken,associationController.update)
router.delete("/delete",verifyToken,associationController.delete)
router.get("/all",verifyToken,associationController.getAll)
router.post("/getByLastName",verifyToken,associationController.getByLastName)

router.post("/auth",associationController.auth)
module.exports=router


