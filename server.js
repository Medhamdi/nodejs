var express= require("express")
var cors= require("cors")
var multer= require('multer')
var fs= require("fs") 
const upload= multer({dest: __dirname + '/uploads/images'});



var bodyParser=require("body-parser")

var associationRouter=require("./routes/associationRouter")
var benevoleRouter=require("./routes/benevoleRouter")
var adminRouter=require("./routes/adminRouter")
var donRouter=require("./routes/donRouter")



var app=express();
app.use(cors())
app.set("secretkey","mohamed")
app.use(bodyParser.json())
app.use("/association",associationRouter)
app.use("/benevole",benevoleRouter)
app.use("/admin",adminRouter)
app.use("/don",donRouter)

app.get("/",function(req,res) {
   res.send("welcome to my server")

})





app.get("/home",function(req,res) {
    res.send("welcome to my home")
 
 })

 app.post('/file_upload', upload.single("file"), function (req, res) {

    var file = __dirname + '/uploads/images' + req.file.originalname;

    fs.readFile(req.file.path, function (err, data) {


        fs.writeFile(file, data, function (err) {
            if (err) {
                console.error(err);
                response = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
            } else {
                response = {
                    message: 'File uploaded successfully',
                    filename: req.file.originalname
                };
            }
            res.end(JSON.stringify(response));
        });
    });
})
 


app.listen(3003,function(){
    console.log("start");
})