module.exports = (sequelize, type) => {
    return sequelize.define('association', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        Nom: type.STRING,
        Email: type.STRING,
        Tel: type.INTEGER,
        Adresse: type.STRING,
        Ville: type.STRING,
        President: type.STRING,
        Description: type.STRING,
        password: type.STRING
        
    })
}