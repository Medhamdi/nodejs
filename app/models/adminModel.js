module.exports = (sequelize, type) => {
    return sequelize.define('admin', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        Nom: type.STRING,
        Prenom: type.STRING,
        Email: type.STRING,
        password: type.STRING
    })
}