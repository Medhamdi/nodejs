module.exports = (sequelize, type) => {
    return sequelize.define('benevole', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        Nom: type.STRING,
        Prenom: type.STRING,
        Email: type.STRING,
        Tel: type.INTEGER,
        Ville: type.STRING,
        Adresse: type.STRING,
        password: type.STRING

    })
}