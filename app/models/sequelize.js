

const Sequelize = require('sequelize')


//User.prototype.generateHash = function 

const AssociationModel = require('./associationModel')
const BenevoleModel = require('./benevoleModel')
const AdminModel = require('./adminModel')
const DonModel = require('./donModel')


const sequelize = new Sequelize('monpfe', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    operatorsAliases: false
  })
  
  const Association = AssociationModel(sequelize, Sequelize)
  const Benevole = BenevoleModel(sequelize, Sequelize)
  const Admin = AdminModel(sequelize, Sequelize)
  const Don = DonModel(sequelize, Sequelize)

  // 1:M
  Benevole.hasMany(Don, { foreignKey: 'user_pk' });
  Don.belongsTo(Benevole, { foreignKey: 'user_pk' }); 

  sequelize.sync({ force: true })
  .then(() => {
    console.log(`Database & tables created!`)
  })

module.exports = {
  Admin,
  Association,
  Benevole,
  Don
  

}