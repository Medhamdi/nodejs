var {Association}= require("../models/sequelize")
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
var randtoken = require("rand-token")
var tokenList = {};



module.exports= {

     create: function (req,res,next) {
        Association.create(req.body).then(item => {
            res.send("create association")
        })
        
     },

     delete: function (req,res,next) {
         Association.destroy({
             where: {
                 id:req.query.id
             }        

         }).then(item=> {
             console.log(item)
             res.send("okkk")
         });

    
     },
     getAll: function (req,res,next) {
        Association.findAll({
           }
        
             ).then(items=> {
               res.send(items)
           })
            
    
    },

     getByLastName: function (req,res,next) {
       Association.findAll({
        where:
        {
            Type: req.body.Type
        }

       }
    
         ).then(items=> {
           res.send(items)
       })
        
     },

     update: function (req,res,next) {
         Association.update(
             req.body,
             { where: { id: req.query.id } }

             
         )
            .then(result =>
            res.send("ok update"))
            .catch(err=> 
            res.send('ok error'))
    
     

    },
    auth: function (req, res, next) {

        Association.findAll({
                where:
                    {
                        Email: req.body.Email
                    }
                                            
            }
        ).then(items => {
    
            // res.send(items[0])
    
            console.log(items[0].password)
    
            if (bcrypt.compareSync(req.body.password, items[0].password)) {
    
    
                const token = jwt.sign({id: items[0].id},req.app.get("secretkey"), {expiresIn: '1h'});
    
                var refreshToken = randtoken.uid(256)
                tokenList[refreshToken] = items[0].id
                console.log({
                    status: "success",
                    message: "user found!!!",
                    data: {user: items[0], token: token, refreshToken: refreshToken}
                })
    
                res.json({
                    status: "success",
                    message: "user found!!!",
                    data: {user: items[0], token: token, refreshToken: refreshToken}
                });
    
            } else {
                res.json({status: "error", message: "Invalid email/password!!!", data: null});
            }
    
    
        }).catch(err => {
    
            console.log("err",err);
    
            res.send(err)
        })
    }
}

var generateHash = function (password) {

    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

};



