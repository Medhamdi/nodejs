var {Admin}= require("../models/sequelize")
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
var randtoken = require("rand-token")
var tokenList = {};


module.exports= {

     create: function (req,res,next) {
        var hashPassword = generateHash(req.body.password)
        Admin.create({
              Nom: req.body.Nom,    
              Prenom: req.body.Prenom,    
              Email: req.body.Email, 
              password: hashPassword
        })        .then(item => {
            res.send({state:"yes",msg:"create admin"})
        })
        
     },

    





     delete: function (req,res,next) {
        Admin.destroy({
             where: {
                 id:req.query.id
             }        
             
         }).then(item=> {
             console.log(item)
             if(item==7)
             res.send({"state":"yes"})
             else
             res.send({"state":"no"})
         }).catch(
             err=> {

                res.send({"state":"no"})
             }
         )
         ;

    
     },
     getAll: function (req,res,next) {
        Admin.findAll({
           }
        
             ).then(items=> {
               res.send(items)
           })
            
    
    },

     getByLastName: function (req,res,next) {
        Admin.findAll({
        where:
        {
            Name: req.body.Name
        }

       }
    
         ).then(items=> {
           res.send(items)
       })
        
     },

     update: function (req,res,next) {
        var hashPassword = generateHash(req.body.password)
        Admin.update(
            {
            Nom: req.body.Nom,    
            Prenom: req.body.Prenom,    
            Email: req.body.Email, 
            password: hashPassword
            },
             { where: { id: req.query.id } }

             
         )
            .then(result =>
            res.send("ok update"))
            .catch(err=> 
            res.send('ok error'))
    
     

    },

    auth: function (req, res, next) {

        Admin.findAll({
                where:
                    {
                        Email: req.body.Email
                    }
    
            }
        ).then(items => {
    
            // res.send(items[0])
    
            console.log(items[0].password)
    
            if (bcrypt.compareSync(req.body.password, items[0].password)) {
    
    
                const token = jwt.sign({id: items[0].id},req.app.get("secretkey"), {expiresIn: '1h'});
    
                var refreshToken = randtoken.uid(256)
                tokenList[refreshToken] = items[0].id
                console.log({
                    status: "success",
                    message: "user found!!!",
                    data: {user: items[0], token: token, refreshToken: refreshToken}
                })
    
                res.json({
                    status: "success",
                    message: "user found!!!",
                    data: {user: items[0], token: token, refreshToken: refreshToken}
                });
    
            } else {
                res.json({status: "error", message: "Invalid email/password!!!", data: null});
            }
    
    
        }).catch(err => {
    
            console.log("err",err);
    
            res.send(err)
        })
    }

}





var generateHash = function (password) {

    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

};




